#include "socket.h"
#include <string>
#include <iostream>

using namespace std;

void print_error_state(string msg, SOCKET_STATE state) {
    cout << "["<< msg <<"]" << " with Error code " << state << endl;
    throw std::runtime_error(msg.c_str());
}

int main() {
    char buffer[4096] = {0};
    string msg = "HIIiiii";

    string ip = "140.112.14.208";
    int port = 12000;
    SOCKET_STATE check;
    SocketClient *s_client = new SocketClient(ip, port);

    check = s_client->initialize();
    if (check != SUCCESS)
        print_error_state("Fail initialize", check);

    check = s_client->connect_to_remote();
    if (check != SUCCESS)
        print_error_state("Fail connect", check);

    check = s_client->recv_data(buffer);
    if (check != SUCCESS)
        print_error_state("Recv error", check);

    cout << buffer << endl;// \x00

    check = s_client->send_data(msg.c_str());
    if (check != SUCCESS)
        print_error_state("Send error", check);
}
