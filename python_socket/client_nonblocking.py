import socket
import time
import select
import traceback

class SocketClient():
    def __init__(self, remote="127.0.0.1", port=8080):
        self.remote = remote
        self.port = port
        self.sock = None

    def initialize(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    def connect(self):
        try:
            self.sock.connect((self.remote, self.port))
            self.sock.setblocking(False)
            # self.sock.settimeout(2)
            print(f"Sucesssfully connect to {self.remote} {self.port}")
        except:
            traceback.print_exc()

    def recv_text(self):
        readable, _, _ = select.select([self.sock], [], [], 2)
        if readable:
            msg = self.sock.recv(4096) # blocking _> wait
            print("get: ", msg)
        else:
            print("not ready")

if __name__ == "__main__":
    socket_client = SocketClient(remote="127.0.0.1", port=9999)
    socket_client.initialize()
    socket_client.connect()
    time.sleep(100)
    socket_client.recv_text()

